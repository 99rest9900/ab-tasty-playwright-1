import { Locator, Page } from '@playwright/test';

export class LoginPage {
  readonly page: Page;

  constructor(page: Page) {
    this.page = page;
  }

  get loginForm(): Locator {
    return this.page.locator('[class^="Layout__leftColumnInside"]');
  }

  get title(): Locator {
    return this.page.locator('h1');
  }

  get signInBtn(): Locator {
    return this.page.locator('[type="submit"]');
  }

  get ssoLoginBtn(): Locator {
    return this.page.locator('[data-testid="keyIcon"]');
  }

  get ssoHeader(): Locator {
    return this.page.locator('[class^="SSOLoginPage__header"]');
  }
}

export default LoginPage;
