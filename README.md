# Visual regression automation

[Playwright documentation](https://playwright.dev/docs/intro)

To install dependencies
> npm install

To install browsers 
> npx playwright install

To run tests
> npm run test:run
or
> npx playwright test

To update screenshots
> npm run test:update
or
> npx playwright test -u

To run in headed mode
> npx playwright test --headed