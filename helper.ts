import { Locator, LocatorScreenshotOptions, expect } from '@playwright/test';

export const screenshotToPass = async (
  locator: Locator,
  screenName: string,
  options: LocatorScreenshotOptions = { animations: 'disabled' },
  timeout = 10000
) => {
  await expect(async () => {
    const screen = await locator.screenshot(options);
    expect(screen).toMatchSnapshot({
      name: screenName,
      threshold: 0.3,
    });
  }).toPass({
    intervals: [2000, 2000, 2000, 2000],
    timeout,
  });
};
