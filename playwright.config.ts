import { defineConfig, devices } from '@playwright/test';

export default defineConfig({
  testDir: './tests',
  snapshotDir: `refs`,
  fullyParallel: true,
  timeout: 15 * 1000,
  expect: {
    timeout: 5000,
  },
  /* Retry on CI only */
  retries: 1,
  /* Opt out of parallel tests on CI. */
  workers: 1,
  /* Reporter to use. See https://playwright.dev/docs/test-reporters */
  reporter: [
    ['html'],
    ['junit', { outputFile: 'playwright-report/xunit.xml' }],
    ['json', { outputFile: 'playwright-report/report.json' }],
  ],
  use: {
    baseURL: 'https://app2.abtasty.com',
    trace: 'on-first-retry',
    headless: true,
  },

  /* Configure projects for major browsers */
  projects: [
    {
      name: 'english',
      use: {
        locale: 'en-GB',
        ...devices['Desktop Chrome'],
      },
    },
    {
      name: 'french',
      use: {
        locale: 'fr-FR',
        ...devices['Desktop Chrome'],
      },
    },
    {
      name: 'german',
      use: {
        locale: 'de-DE',
        ...devices['Desktop Chrome'],
      },
    },
    {
      name: 'spain',
      use: {
        locale: 'es-ES',
        ...devices['Desktop Chrome'],
      },
    },
    {
      name: 'safari-mobile',
      use: { ...devices['iPhone 13 Pro'] },
    },
    {
      name: 'webkit',
      use: { ...devices['Desktop Safari'] },
    },
  ],
});

