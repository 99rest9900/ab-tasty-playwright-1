import { test, expect, Page } from '@playwright/test';
import LoginPage from '../page-objects/login-page';

test.describe('Login form', () => {
let page: Page;
let loginPage: LoginPage;

  test.beforeAll(async ({ browser, baseURL }) => {
    const context = await browser.newContext();
    page = await context.newPage();
    loginPage = new LoginPage(page);
    await page.goto(`${baseURL}/login`);
  });

  test('Default main login form', async () => {
    await loginPage.title.waitFor();
    await loginPage.signInBtn.waitFor();
    await page.waitForTimeout(2000);
    const screen = await loginPage.loginForm.screenshot({
      animations: 'disabled',
    });
    expect(screen).toMatchSnapshot({
      name: 'default-login-form.png',
      threshold: 0.3,
    });
  });

  test('Sso login form', async () => {
    await loginPage.ssoLoginBtn.click();
    await loginPage.ssoHeader.waitFor();
    await page.waitForTimeout(2000);
    const screen = await loginPage.loginForm.screenshot({
      animations: 'disabled',
    });
    expect(screen).toMatchSnapshot({
      name: 'sso-login-form.png',
      threshold: 0.3,
    });
  });
});